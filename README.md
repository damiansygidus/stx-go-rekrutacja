# HTTP request handle with JSON response in Go

### To run:
```
go build
./go-rekrutacja
```


### Example to test:
```
curl -i http://localhost:3000/\?field1\=value1\&field2\=value2\&field3\=value3\+longer\+than\+others
```