package main

import (
	"fmt"
	"encoding/json"
  "net/http"
)

func main() {
  http.HandleFunc("/", handler)
	http.ListenAndServe(":3000", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	empData, err := json.Marshal(r.URL.Query())
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	
	fmt.Fprintf(w, string(empData))
}